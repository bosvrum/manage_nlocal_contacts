class Api::V1::ContactSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :email
  has_one :user, serializer: Api::V1::UserSerializer
end