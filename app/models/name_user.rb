class NameUser
  attr_reader :name, :email, :password, :password_confirmation
  def initialize(name, email, password, password_confirmation)
    @name = name
    @email = email
    @password = password
    @password_confirmation = password_confirmation
  end

  def first_name
    name.split(" ").first
  end

  def last_name
    name.split(' ').drop(1).join(' ')
  end
end