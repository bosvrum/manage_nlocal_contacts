class Contact < ActiveRecord::Base
  include Filterable
  delegate :full_name, to: :contact_methods
  mount_uploader :image, ContactImageUploader

  # Add validations
  validates :first_name, :last_name, :email, presence: true
  validates_uniqueness_of :email
         
  # Add associations
  belongs_to :user     
  has_many :phones, dependent: :destroy 
  accepts_nested_attributes_for :phones     

  scope :keyword, -> (keyword) { where("first_name LIKE ?", "%#{keyword}%")}  
  #scope :keyword, -> (keyword) { where("last_name LIKE ?", "%#{keyword}%")}         

  def contact_methods
    ContactMethods.new(first_name, last_name, email)
  end

  
end
