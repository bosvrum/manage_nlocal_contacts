class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  delegate :first_name, :last_name, to: :name_user        
  validates :name, :email, :password, presence: true        
  #Add Associations
  has_many :contacts 

  def name_user
    NameUser.new(name, email, password, password_confirmation)      
  end       
end
