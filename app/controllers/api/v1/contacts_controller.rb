class Api::V1::ContactsController < ApplicationController
  protect_from_forgery with: :null_session

  before_action :destroy_session

  def index
    render(
      json: Contact.all,
      each_serializer: Api::V1::ContactSerializer
    )
  end
  def show
    contact = Contact.find(params[:id])
    render(json: Api::V1::ContactSerializer.new(contact).to_json)
  end

  def destroy_session
    request.session_options[:skip] = true
  end
end