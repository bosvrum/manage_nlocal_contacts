class Api::V1::UsersController < ApplicationController
  def index
    render(
      json: User.all,
      each_serializer: Api::V1::UserSerializer
    )
  end

  def show
    user = User.find(params[:id])

    render(json: Api::V1::UserSerializer.new(user).to_json)
  end
end