FactoryGirl.define do
  factory :contact do
    first_name "Radu"
    last_name "Tata"
    email "radu@example.com"
  end
end
