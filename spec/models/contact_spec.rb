require 'rails_helper'

RSpec.describe Contact, type: :model do
  it "knows full names" do
    contact = Contact.create(:first_name => "Cata", :last_name => "Lin") 
    expect(contact.full_name).to eq("Cata Lin")
  end
  #validations
  context 'Contact Validations' do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:email) }
  end
  # validate uniqueness of email
  context 'Email is unique' do
    it { should validate_uniqueness_of(:email) }
  end

  context 'ContactMethods delegate' do
    it { should delegate_method(:full_name).to(:contact_methods) }
  end
  context 'Validate associations' do
    it { should belong_to(:user) }
    it { should have_many(:phones) }
  end
  
end
