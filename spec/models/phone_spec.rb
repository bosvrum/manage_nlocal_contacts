require 'rails_helper'

RSpec.describe Phone, type: :model do
  context 'Validate associations' do
    it { should belong_to(:contact) }
  end
end
