require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Validate associations' do
    it { should have_many(:contacts) }
  end
end
