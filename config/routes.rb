Rails.application.routes.draw do
  root to: "homes#show"
  devise_for :users
  resources :contacts
  namespace :api do
    namespace :v1 do
      resources :contacts, only:[:index, :show]
      resources :users, only:[:index,:show]
    end
  end
end
